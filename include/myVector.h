#pragma once

/**
 * !brief The aim of this class is to provide a simple vector component managing only floats values.
 */
class myVector final {
public :
    /**
     * @brief Construct a new my Vector object
     * 
     * @param numberOfItems Number of maximum number of items in te vector 
     */
    myVector (const int numberOfItems);

    /**
     * @brief Destroy the my Vector object
     * 
     */
    ~myVector();

    /**
     * @brief Get item value at given position
     * 
     * @param itemPosition The position on witch the requested value is
     * @return float
     */
    float get(const int itemPosition);

    /**
     * @brief Set the value of an item in the vector at given position
     * 
     * @param itemPosition The item on witch the given value will be added
     * @param value The value to put
     */
    void set(const int itemPosition, const float value);

    /**
     * @brief Add the given vector to the current one. 
     * @warning The two vectors must have the same size
     * 
     * @param other The vector to add to the current one
     */
    void operator+=(const myVector& other);

    /**
     * @brief Add two different vectors.
     * @warning The two vectors must have the same size
     * 
     * @param other The vector to add to the current one
     * @return myVector 
     */
    myVector operator+(const myVector& other);

    /**
     * @brief remove a value from the vector and replace it with NAN
     * 
     * @param itemPosition The postion of the item to remove
     */
    void remove(const int itemPosition);

private:
    /**
     * @brief The stored value of the vector
     */
    
    /**
     * @brief The vector size
     * 
     */
    int vectorSize;

//Tests
    friend class myVectorTest_constructor_HappyPath_Test;
    friend class myVectorTest_constructor_NegativeValue_Test;
    friend class myVectorTest_constructor_NullValue_Test;
    
};

