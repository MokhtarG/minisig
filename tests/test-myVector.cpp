#include "gtest/gtest.h"
#include "myVector.h"

TEST(myVectorTest, constructor_HappyPath) {
    //arrange
    const int nbItems(10);
    
    //act
    myVector vect(nbItems);
    
    //assert
    EXPECT_EQ (nbItems,  vect.vectorSize);
}

TEST(myVectorTest, constructor_NegativeValue) {
    //arrange
    const int nbItems(-1);
    
    //act
    myVector vect(nbItems);
    
    //assert
    EXPECT_EQ (1,  vect.vectorSize);
}

TEST(myVectorTest, constructor_NullValue) {
    //arrange
    const int nbItems(0);
    
    //act
    myVector vect(nbItems);
    
    //assert
    EXPECT_EQ (1,  vect.vectorSize);
}